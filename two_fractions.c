#include<stdio.h>
typedef struct
{
	int num;
	int deno;
} fraction;
fraction input(int n)
{
	fraction x;
	printf("Enter the numerator for %d fraction",n);
	scanf("%d",&x.num);
	printf("Enter the denominator for %d fraction:\n",n);
	scanf("%d",&x.deno);
	return x;
}
int compute_gcd(int a, int b)
{
	int hcf;
	for(int i=1;i<=a && i<=b;i++)
	{
	    if(a%i==0 && b%i==0)
	    {
	        hcf=i;
	    }
	}
	return hcf;
}
fraction simplify(fraction sum)
{
	int gcd=compute_gcd(sum.num,sum.deno);
	sum.num=sum.num/gcd;
	sum.deno=sum.deno/gcd;
	return sum;
}
fraction compute_sum(fraction f1, fraction f2)
{
    fraction sum;
    sum.num=(f1.num*f2.deno)+(f2.num*f1.deno);
    sum.deno=f1.deno*f2.deno;
    return sum;
}
void output(fraction f1, fraction f2, fraction sum)
{
	printf("The sum of the two fractions %d/%d and %d/%d is: %d/%d.\n",f1.num,f1.deno,f2.num,f2.deno,sum.num,sum.deno);
}
int main()
{
	fraction f1,f2,sum;
	f1=input(1);
	f2=input(2);
	sum=compute_sum(f1,f2);
	sum=simplify(sum);
	output(f1,f2,sum);
	return 0;
}
	