#include<stdio.h>
float input(char a)
{
	float n;
	printf("Enter the value of %c :\n",a);
	scanf("%f", &n);
	return n;
}

float compute_volume(float h, float b, float d)
{
	float volume = (float)1/3*((h*d)+d)/b;
	return volume;
}

void output(float v)
{
	printf("The volume of the given tromboid is %f \n", v);
}

int main()
{
	float h,b,d,v;
	h = input('h');
	b = input('b');
	d = input('d');
	v = compute_volume(h, b, d);
	output(v);
	return 0;
}
