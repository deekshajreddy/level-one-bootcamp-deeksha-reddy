#include<stdio.h>
float input()
{
	float n;
	printf("Enter number: \n");
	scanf("%f", &n);
	return n;
}
float sum(float n1, float n2)
{
	float sum;
	sum = n1+n2;
	return sum;
}
void output(float n)
{
	printf("The sum of x and y is %f\n",n);
}
int main()
{
	float x,y,z;
	x = input();
	y = input();
	z = sum(x,y);
	output(z);
	return 0;
}